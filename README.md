# heart_failure_prediction analysis
Tento projekt je součásti semestrální práce z předmětu Základy datových analýz.
# Obsah

- **složka tables** obsahuje všechny tabulky, které byly použity pro analýzu, včetně původní tabulky a nově vytvořených během práce.

- **složka heart_failure_prediction** obsahuje všechny vytvořené Python skripty a csv files

- **report** týkající se práce na projektu.
