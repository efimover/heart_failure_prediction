import pandas as pd
import seaborn as sns
import statsmodels.api as sm
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score

# Load the CSV file into a DataFrame
df = pd.read_csv('/home/veronika/Desktop/FEL/ZDA/semestralka/heart_failure_clinical_records_dataset.csv')
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
# Display the first few rows of the DataFrame
print(df.head(15))

# Get basic information about the dataset
print(df.info())

# Get summary statistics for numerical columns
print(df.describe())

# Check for missing values
print(df.isnull().sum())

df = df.dropna()

# Display basic statistics again to confirm
print(df.describe())


# Filter patients with diabetes and high blood pressure
diabetes_hbp = df[(df['diabetes'] == 1) & (df['high_blood_pressure'] == 1)]

print(diabetes_hbp)

diabetes_hbp.to_csv('diabetes_hbp.csv', index=False)
# Calculate the mortality rate for these patients
mortality_rate_diabetes_hbp = diabetes_hbp['DEATH_EVENT'].mean()
# Assuming 'DEATH_EVENT' is the target column indicating death (1 for death, 0 for survival)
print(f"Mortality rate for patients with diabetes and high blood pressure: {mortality_rate_diabetes_hbp:.2f}")

# For comparison, calculate the overall mortality rate
overall_mortality_rate = df['DEATH_EVENT'].mean()
print(f"Overall mortality rate: {overall_mortality_rate:.2f}")

women_diabetes_hbp = df[(df['sex'] == 1) & (df['diabetes'] == 1) & (df['high_blood_pressure'] == 1)]
women_diabetes_hbp_sorted = women_diabetes_hbp.sort_values(by=['age'])

# Filter and sort data for men with diabetes and high blood pressure
men_diabetes_hbp = df[(df['sex'] == 0) & (df['diabetes'] == 1) & (df['high_blood_pressure'] == 1)]
men_diabetes_hbp_sorted = men_diabetes_hbp.sort_values(by=['age'])

# Calculate mortality rates
mortality_rate_women_diabetes_hbp = women_diabetes_hbp_sorted['DEATH_EVENT'].mean()
mortality_rate_men_diabetes_hbp = men_diabetes_hbp_sorted['DEATH_EVENT'].mean()

women_diabetes_hbp_sorted.to_csv('women_diabetes_hbp_sorted.csv', index=False)
men_diabetes_hbp_sorted.to_csv('men_diabetes_hbp_sorted.csv', index=False)

# Display sorted tables and mortality rates
print("Sorted table for women with diabetes and high blood pressure:")
print(women_diabetes_hbp_sorted)
print(f"\nMortality rate for women with diabetes and high blood pressure: {mortality_rate_women_diabetes_hbp:.2f}")

print("\nSorted table for men with diabetes and high blood pressure:")
print(men_diabetes_hbp_sorted)
print(f"\nMortality rate for men with diabetes and high blood pressure: {mortality_rate_men_diabetes_hbp:.2f}")


sns.set_style("whitegrid")

# Visualization 1: Age Distribution for Patients with Diabetes and High Blood Pressure
plt.figure(figsize=(10, 6))
sns.histplot(data=diabetes_hbp, x='age', kde=True, bins=20, color='skyblue', edgecolor='black')
plt.title('Age Distribution for Patients with Diabetes and High Blood Pressure')
plt.xlabel('Age')
plt.ylabel('Frequency')
plt.show()

# Visualization 2: Mortality Rate Comparison between Women and Men with Diabetes and High Blood Pressure
plt.figure(figsize=(8, 6))
sns.barplot(x=['Women', 'Men'], y=[mortality_rate_women_diabetes_hbp, mortality_rate_men_diabetes_hbp], hue=['Women', 'Men'], palette='pastel', legend=False)
plt.title('Mortality Rate Comparison between Women and Men with Diabetes and High Blood Pressure')
plt.ylabel('Mortality Rate')
plt.show()

# Visualization 3: Mortality Rate Comparison between Patients with and without Diabetes
# Plot mortality rate comparison between patients with and without diabetes
plt.figure(figsize=(8, 6))
sns.barplot(x=['With Diabetes', 'Without Diabetes'], y=[mortality_rate_diabetes_hbp, overall_mortality_rate], hue=['With Diabetes', 'Without Diabetes'], palette='pastel', legend=False)
plt.title('Mortality Rate Comparison between Patients with and without Diabetes')
plt.ylabel('Mortality Rate')
plt.show()
#
# Prepare the data for regression
X = df.drop(columns='DEATH_EVENT')
y = df['DEATH_EVENT']

# Add a constant to the model (intercept)
X = sm.add_constant(X)

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Fit the linear regression model on the training data
model = sm.OLS(y_train, X_train).fit()

# Print the model summary
print(model.summary())

# Make predictions on the testing data
y_pred = model.predict(X_test)

# Evaluate the model's performance
mse = mean_squared_error(y_test, y_pred)
r2 = r2_score(y_test, y_pred)
print(f'Mean Squared Error: {mse}')
print(f'R-squared: {r2}')

# Visualization: Actual vs Predicted values
plt.figure(figsize=(10, 6))
plt.scatter(y_test, y_pred)
plt.xlabel('Actual Death Events')
plt.ylabel('Predicted Death Events')
plt.title('Actual vs Predicted Death Events')
plt.plot([0, 1], [0, 1], color='red', linestyle='--', linewidth=2)
plt.show()

# Analyze the predictions for patients with diabetes and high blood pressure
diabetes_hbp_test = X_test[(X_test['diabetes'] == 1) & (X_test['high_blood_pressure'] == 1)]
y_test_diabetes_hbp = y_test.loc[diabetes_hbp_test.index]
y_pred_diabetes_hbp = y_pred.loc[diabetes_hbp_test.index]

# Evaluate the model's performance on this subset
mse_diabetes_hbp = mean_squared_error(y_test_diabetes_hbp, y_pred_diabetes_hbp)
r2_diabetes_hbp = r2_score(y_test_diabetes_hbp, y_pred_diabetes_hbp)
print(f'Mean Squared Error (Diabetes & High Blood Pressure): {mse_diabetes_hbp}')
print(f'R-squared (Diabetes & High Blood Pressure): {r2_diabetes_hbp}')

# Visualization: Actual vs Predicted values for Diabetes & High Blood Pressure subset
plt.figure(figsize=(10, 6))
plt.scatter(y_test_diabetes_hbp, y_pred_diabetes_hbp)
plt.xlabel('Actual Death Events')
plt.ylabel('Predicted Death Events')
plt.title('Actual vs Predicted Death Events (Diabetes & High Blood Pressure)')
plt.plot([0, 1], [0, 1], color='red', linestyle='--', linewidth=2)
plt.show()


def perform_linear_regression(df, predictor_vars, outcome_var):
    # Prepare the predictor variables (X) and the outcome variable (y)
    X = df[predictor_vars]
    y = df[outcome_var]

    # Add a constant to the predictor variables (intercept)
    X = sm.add_constant(X)

    # Fit the linear regression model
    model = sm.OLS(y, X).fit()

    # Print the model summary
    print(model.summary())


# Example usage:
# Define the predictor variables (diabetes, high blood pressure) and the outcome variable (mortality)
predictor_vars = ['diabetes', 'high_blood_pressure']
outcome_var = 'DEATH_EVENT'

# Perform linear regression analysis for all data
perform_linear_regression(df, predictor_vars, outcome_var)

# Perform linear regression analysis for data where diabetes is present and high blood pressure is absent
subset1 = df[(df['diabetes'] == 1) & (df['high_blood_pressure'] == 0)]
perform_linear_regression(subset1, predictor_vars, outcome_var)

# Perform linear regression analysis for data where diabetes is absent and high blood pressure is present
subset2 = df[(df['diabetes'] == 0) & (df['high_blood_pressure'] == 1)]
perform_linear_regression(subset2, predictor_vars, outcome_var)

# Perform linear regression analysis for data where both diabetes and high blood pressure are present
subset3 = df[(df['diabetes'] == 1) & (df['high_blood_pressure'] == 1)]
perform_linear_regression(subset3, predictor_vars, outcome_var)